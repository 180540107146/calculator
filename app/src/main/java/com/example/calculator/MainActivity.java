package com.example.calculator;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    TextView
            tvRow2Column1, tvRow2Column2, tvRow2Column3, tvRow2Column4, tvRow2Column5, tvRow2Column6,
            tvRow3Column1, tvRow3Column2, tvRow3Column3, tvRow3Column4,
            tvRow4Column1, tvRow4Column2, tvRow4Column3, tvRow4Column4,
            tvRow5Column1, tvRow5Column2, tvRow5Column3, tvRow5Column4,
            tvRow6Column1, tvRow6Column2, tvRow6Column3, tvRow6Column4,
            tvRow7Column1, tvRow7Column2, tvRow7Column3, tvRow7Column4,
            tvRow8Column1, tvRow8Column2, tvRow8Column3, tvRow8Column4,
            edt1, tvActDisplayScreenRow1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvRow2Column1 = (TextView) findViewById(R.id.tvActRow2Column1);
        tvRow2Column2 = (TextView) findViewById(R.id.tvActRow2Column2);
        tvRow2Column3 = (TextView) findViewById(R.id.tvActRow2Column3);
        tvRow2Column4 = (TextView) findViewById(R.id.tvActRow2Column4);
        tvRow2Column5 = (TextView) findViewById(R.id.tvActRow2Column5);
        tvRow2Column6 = (TextView) findViewById(R.id.tvActRow2Column6);

        tvRow3Column1 = (TextView) findViewById(R.id.tvActRow3Column1);
        tvRow3Column2 = (TextView) findViewById(R.id.tvActRow3Column2);
        tvRow3Column3 = (TextView) findViewById(R.id.tvActRow3Column3);
        tvRow3Column4 = (TextView) findViewById(R.id.tvActRow3Column4);

        tvRow4Column1 = (TextView) findViewById(R.id.tvActRow4Column1);
        tvRow4Column2 = (TextView) findViewById(R.id.tvActRow4Column2);
        tvRow4Column3 = (TextView) findViewById(R.id.tvActRow4Column3);
        tvRow4Column4 = (TextView) findViewById(R.id.tvActRow4Column4);

        tvRow5Column1 = (TextView) findViewById(R.id.tvActRow5Column1);
        tvRow5Column2 = (TextView) findViewById(R.id.tvActRow5Column2);
        tvRow5Column3 = (TextView) findViewById(R.id.tvActRow5Column3);
        tvRow5Column4 = (TextView) findViewById(R.id.tvActRow5Column4);

        tvRow6Column1 = (TextView) findViewById(R.id.tvActRow6Column1);
        tvRow6Column2 = (TextView) findViewById(R.id.tvActRow6Column2);
        tvRow6Column3 = (TextView) findViewById(R.id.tvActRow6Column3);
        tvRow6Column4 = (TextView) findViewById(R.id.tvActRow6Column4);

        tvRow7Column1 = (TextView) findViewById(R.id.tvActRow7Column1);
        tvRow7Column2 = (TextView) findViewById(R.id.tvActRow7Column2);
        tvRow7Column3 = (TextView) findViewById(R.id.tvActRow7Column3);
        tvRow7Column4 = (TextView) findViewById(R.id.tvActRow7Column4);

        tvRow8Column1 = (TextView) findViewById(R.id.tvActRow8Column1);
        tvRow8Column2 = (TextView) findViewById(R.id.tvActRow8Column2);
        tvRow8Column3 = (TextView) findViewById(R.id.tvActRow8Column3);
        tvRow8Column4 = (TextView) findViewById(R.id.tvActRow8Column4);


        tvRow2Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvRow2Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvRow2Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvRow2Column4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvRow2Column5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvRow2Column6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvRow5Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "7");
            }
        });

        tvRow5Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "8");
            }
        });

        tvRow5Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "9");
            }
        });

        tvRow5Column4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "*");
            }
        });




        tvRow6Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "4");
            }
        });

        tvRow6Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "5");
            }
        });

        tvRow6Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "6");
            }
        });

        tvRow6Column4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "-");
            }
        });




        tvRow7Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "1");
            }
        });

        tvRow7Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "2");
            }
        });

        tvRow7Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "3");
            }
        });

        tvRow7Column4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "+");
            }
        });




        tvRow8Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "+/-");
            }
        });

        tvRow8Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "0");
            }
        });

        tvRow8Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean decimal = false;
                if (decimal) {

                } else {
                    edt1.setText(edt1.getText() + ".");
                    decimal = true;
                }

            }
        });
        tvRow4Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "1/x");
            }
        });

        tvRow4Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "x^2");
            }
        });

        tvRow4Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "√x");
            }
        });

        tvRow4Column4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "/");
            }
        });

        tvRow3Column1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "%");
            }
        });

        tvRow3Column2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("");
            }
        });

        tvRow3Column3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("");
            }
        });


        tvRow8Column4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "=");
            }
        });

        tvRow3Column4.setOnClickListener(new View.OnClickListener() {
            private final TextView DONT_CLEAR = null;

            @Override
            public void onClick(View v) {
                if (edt1 != null) {
                    tvActDisplayScreenRow1 = DONT_CLEAR;
                    String str = edt1.getText().toString();
                    if (str.length() > 1) {
                        str = str.substring(0, str.length() - 1);
                        edt1.setText(str);
                    } else if (str.length() <= 1) {
                        edt1.setText("");
                    }
                }

            }
        });


        edt1 = (TextView) findViewById(R.id.tvActDisplayScreenRow1);

    }
}
